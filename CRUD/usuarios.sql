-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2019 a las 15:38:36
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usuarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(150) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `cantidad` int(10) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre_producto`, `descripcion`, `cantidad`, `precio`) VALUES
(1, 'Monitor viewsonic gamer', 'color negro 24\"', 3, 120),
(2, 'Teclado redragon gamer', '261 teclas color negro', 5, 22),
(3, 'Monitor LG1', 'Monitor LG 24', 4, 125000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `rut` varchar(12) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `fecha_nac` date NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`rut`, `nombre`, `pass`, `fecha_nac`, `correo`, `telefono`) VALUES
('171077907', 'Jose Cerda1', 'd41d8cd98f00b204e9800998ecf8427e', '2019-04-01', 'jose.cerda66@gmail.com', 953513634),
('9029', 'Karma', 'd14c2267d848abeb81fd590f371d39bd', '2019-04-18', 'ban.albrecht.18@gmail.com', 31918855),
('20028477-1', 'Jose Gutierrez', 'f717c413cebc560ff181002dbc323431', '1999-04-01', 'josemiguelgutierrezaliagagmail.com', 42506974),
('192612535', 'matias lagos', '6403675579f6114559c90de0014cd3d6', '1996-02-26', 'zenaku@gmail.com', 0),
('1-9', 'alonso', '68053af2923e00204c3ca7c6a3150cf7', '2019-04-12', 'c@c.cl', 133);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
